import numpy as np
import csv
import argparse
import time
from classes.csv_user import CsvUser


def csvify(users):

    multiuser_array = [["first", "last", "email", "position"]]
    while users:
        user_array = []
        new_user = CsvUser()
        for value in new_user.__dict__.values():
            user_array.append(value)
        multiuser_array.append(user_array)
        users -= 1

    multiuser_array = np.array(multiuser_array)
    csv_file = f"data/fakeusers_{time.ctime()[4:19].replace(' ', '_').replace(':', '_')}.csv"
    with open(csv_file, "w", newline='') as user_file:
        writer = csv.writer(user_file)
        writer.writerows(multiuser_array)

    print(f"Users written to {csv_file}")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Set args for user generation"
    )
    parser.add_argument(
        "-u",
        "--users",
        type=int,
        default=1,
        help="Number of users to generate; default 1"
    )
    args = parser.parse_args()
    csvify(users=args.users)
