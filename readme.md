# CSVIFY

## How to use

1. Clone/download
2. Run `pip install -r requirements.txt`
3. Run csvify.py; this takes a `--users` argument for total users and defaults to 1
4. Marvel at the magical appearance of a pre-populated fakeusers csv
