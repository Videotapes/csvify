from mimesis import Person
from mimesis.enums import Gender
from random import choice, choices


class CsvUser:

    def __init__(self):
        self.firstname = Person().name(gender=choice([Gender.MALE, Gender.FEMALE]))
        self.lastname = Person().last_name()
        self.email = f"{self.firstname}.{self.lastname}@example.com"
        for selection in choices(["RN", "CNA", "LPN", "NM"],
                                 [0.75, 0.25, 0.25, 0.10], k=1):
            self.position = selection
